<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('posts', 'Admin\PostsController');

Route::group(['prefix' => 'gerant', 'namespace' => 'Admin', 'middleware' => ['auth', 'auth.gerant']], function() {
    Route::resource('films', 'FilmsController');
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth', 'auth.admin']], function(){

    Route::resource('categories', 'CategoriesController');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
