<?php

namespace App\Http\Controllers\Admin;

use App\Models\Films;
use App\Models\Genre;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = "All films ";
        $films = Films::with('genre')->orderBy('id','DESC')->get();
        return view('films.index', compact('title','films'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genres = Genre::orderBy('nom')->get();
        $title = 'New Film' ;
        return view('films.create', compact('title','genres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // //persistance ds la bd
        Films::create($request->except(['_token']));
        return redirect()->route('films.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Edit film " ;
        $film= Films::find($id);
        if (!$film) return redirect()->route('films.index');
        $genres = Genre::orderBy('nom')->get();
        return view('films.edit', compact('title','genres','film'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $film= Films::find($id);
        if (!$film) return redirect()->route('films.index');
        $film->update($request->except(['_token', '_method'])) ;
        return redirect()->route('films.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if ($film = Films::find($id)){
            $film->delete() ;
        }
        return redirect()->route('films.index');
    }
}
