<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Films extends Model
{
    //
    protected $guarded = ['id'];

    public function genre(){
        return $this->belongsTo('App\Models\Genre');
    }
}
