@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $title }}</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <a href="{{ route('posts.create') }}" class="btn btn-primary">New Post</a>
                        </div>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $p)
                                <tr>
                                    <td>{{ $p->id }}</td>
                                    <td>{{ $p->title }}</td>
                                    <td>{{ $p->category->name }}</td>
                                    <td>
                                        <a href="{{ route('posts.edit', $p->id) }}"
                                           class="btn btn-default btn-xs">Edit</a>
                                        <form action="{{ route('posts.destroy', $p->id) }}" class="form-delete"
                                              method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button class="btn btn-xs btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('scripts')
    <script>
        $(function () {
            $('.form-delete').submit(function (e) {
                if (!confirm('Are you sure to delete this item?')) {
                    e.preventDefault();
                }
            });
        })
    </script>
@stop