@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $title }}</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <a href="{{ route('films.create') }}" class="btn btn-primary">New Film</a>
                        </div>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Genre</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($films as $f)
                                <tr>
                                    <td>{{ $f->id }}</td>
                                    <td>{{ $f->title }}</td>
                                    <td>{{ $f->genre->name }}</td>
                                    <td>
                                        <a href="{{ route('films.edit', $p->id) }}"
                                           class="btn btn-default btn-xs">Edit</a>
                                        <form action="{{ route('films.destroy', $p->id) }}" class="form-delete"
                                              method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button class="btn btn-xs btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('scripts')
    <script>
        $(function () {
            $('.form-delete').submit(function (e) {
                if (!confirm('Are you sure to delete this item?')) {
                    e.preventDefault();
                }
            });
        })
    </script>
@stop