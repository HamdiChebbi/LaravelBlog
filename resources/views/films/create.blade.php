@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $title }}</div>
                    <div class="panel-body">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p>{{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                        <form action="{{ route('films.store') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="">Title</label>
                                <input type="text" name="nom" class="form-control" value="{{ old('nom') }}"
                                       placeholder="Film Title">
                            </div>

                            <div class="form-group">
                                <label for="">Auteur</label>
                                <input type="text" name="auteur" class="form-control" value="{{ old('auteur') }}"
                                       placeholder="auteur du Film ">
                            </div>

                            <div class="form-group">
                                <label for="">Date de sortie</label>
                                <input type="date" name="date_sortie" class="form-control" value="{{ old('date_sortie') }}">
                            </div>
                            <div class="form-group">
                                <label for="">Disponible</label>

                                <input type="number" name="disponible" class="form-control" value="{{ old('disponible') }}">
                            </div>

                            <div class="form-group">
                                <label for="">Genre</label>
                                <select name="genre_id" class="form-control">
                                    <option value="1">Select Gender</option>
                                    @foreach($genres as $g)
                                        <option value="{{ $g->id }}" {{ (old('genre_id') == $g->id) ? 'selected' : '' }}>{{ $g->nom }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop